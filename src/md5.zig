const ossl = @cImport({
    @cInclude("openssl/md5.h");
});

pub const Md5 = struct {
    const Self = @This();
    pub const Options = struct {};
    pub const block_length = 64;
    pub const digest_length = 16;

    ctx: ossl.MD5_CTX,

    pub fn init(options: Options) Self {
        _ = options;
        var c: ossl.MD5_CTX = undefined;
        _ = ossl.MD5_Init(&c);
        return Self{
            .ctx = c,
        };
    }

    pub fn hash(b: []const u8, out: *[digest_length]u8, options: Options) void {
        var d = Md5.init(options);
        d.update(b);
        d.final(out);
    }

    pub fn update(d: *Self, b: []const u8) void {
        _ = ossl.MD5_Update(&d.ctx, b.ptr, b.len);
    }

    pub fn final(d: *Self, out: *[digest_length]u8) void {
        _ = ossl.MD5_Final(out, &d.ctx);
    }
};

const htest = @import("test.zig");

test "md5 single" {
    try htest.assertEqualHash(Md5, "d41d8cd98f00b204e9800998ecf8427e", "");
    try htest.assertEqualHash(Md5, "0cc175b9c0f1b6a831c399e269772661", "a");
    try htest.assertEqualHash(Md5, "900150983cd24fb0d6963f7d28e17f72", "abc");
    try htest.assertEqualHash(Md5, "f96b697d7cb7938d525a2f31aaf161d0", "message digest");
    try htest.assertEqualHash(Md5, "c3fcd3d76192e4007dfb496cca67e13b", "abcdefghijklmnopqrstuvwxyz");
    try htest.assertEqualHash(Md5, "d174ab98d277d9f5a5611c2c9f419d9f", "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");
    try htest.assertEqualHash(Md5, "57edf4a22be3c955ac49da2e2107b67a", "12345678901234567890123456789012345678901234567890123456789012345678901234567890");
}

test "md5 streaming" {
    var h = Md5.init(.{});
    var out: [16]u8 = undefined;

    h.final(out[0..]);
    try htest.assertEqual("d41d8cd98f00b204e9800998ecf8427e", out[0..]);

    h = Md5.init(.{});
    h.update("abc");
    h.final(out[0..]);
    try htest.assertEqual("900150983cd24fb0d6963f7d28e17f72", out[0..]);

    h = Md5.init(.{});
    h.update("a");
    h.update("b");
    h.update("c");
    h.final(out[0..]);

    try htest.assertEqual("900150983cd24fb0d6963f7d28e17f72", out[0..]);
}

test "md5 aligned final" {
    var block = [_]u8{0} ** Md5.block_length;
    var out: [Md5.digest_length]u8 = undefined;

    var h = Md5.init(.{});
    h.update(&block);
    h.final(out[0..]);
}
